# Over The Limit

Ce projet consiste au projet étudiant de 5e session de l'équipe UNDEFINED Studio. C'est un jeu où l'objectif est d'acheter
le plus d'objets possible dans l'objectif de dépasser la limite de sa carte de crédit. Le joueur devra esquiver les commis,
monter des échelles, sauter de plateforme en plateforme et éviter les rabais.

## Démarrage rapide

Ces instructions vous permettront d'obtenir une copie opérationnelle du projet sur votre machine à des fins de développement.

### Prérequis

* [Git](https://git-scm.com/downloads) - Système de contrôle de version. Utilisez la dernière version.
* [Rider](https://www.jetbrains.com/rider/) ou [Visual Studio](https://www.visualstudio.com/fr/) - IDE. Vous pouvez utiliser 
  également n'importe quel autre IDE: assurez-vous simplement qu'il supporte les projets Unity.
* [Unity 2021.3.8f1](https://unity3d.com/fr/get-unity/download/) - Moteur de jeu. Veuillez utiliser **spécifiquement cette 
  version.** Attention à ne pas installer Visual Studio une seconde fois si vous avez déjà un IDE.

**Attention!** Actuellement, seul le développement sur Windows est complètement supporté.

### Compiler une version de développement

Clonez le projet.

```
https://gitlab.com/phil.desbiens/over-the-limit.git
```

Ouvrez le projet dans Unity. 

Cliquez sur *Tools*, *Harmony*, *Code Generator*, *Generate Code*.

Ensuite, ouvrez la scène `Main` et appuyez sur le bouton *Play*.

### Tester un version stable ou de développement

Ouvrez le projet dans Unity. Ensuite, allez dans `File > Build Settings…` et compilez le projet **dans un dossier vide**.

Si vous rencontrez un bogue, vous êtes priés de le [signaler](https://gitlab.com/phil.desbiens/projet-de-developpement/issues/new?issuable_template=Bug).
Veuillez fournir une explication détaillée de votre problème avec les étapes pour reproduire le bogue. Les captures d'écran et 
les vidéos jointes sont les bienvenues.

## Contribuer au projet

Veuillez lire [CONTRIBUTING.md](CONTRIBUTING.md) pour plus de détails sur notre code de conduite.

## Boîte à idées

Si vous avez de nouvelles idées à soumettre à l'équipe de développement, ajoutez-les au document suivant : [Boîte à idées](https://docs.google.com/document/d/13xSN9gWqsjnRsYPy_Ofbg5JL0BHu_Tl5WYAAk7Avruo/edit?usp=sharing)

## Roadmap

Voici la roadmap du projet à suivre : [Roadmap](https://docs.google.com/document/d/1_pLfFb8Jma699XEzvZWLKO_mEFru2qkhbt9NrZ5Vno0/edit?usp=sharing)

## Auteurs

* **Philippe Desbiens** - *Programmeur*
* **Guillaume Thivierge** - *Programmeur*

## Remerciements

* **Benjamin Lemelin** - Extensions sur le moteur Unity pour la recherche d'objets et de composants. Générateur de constantes. Gestionnaire de chargement des scènes.
* **Daniel Huot** - Aide au scrum et à l'organisation de l'équipe.
* **Danny Groleau** - Mentor de l'équipe.
* **Rosalie Lacroix** - *Graphiste*
  * Logo du projet.

## Équipe originale
* **Kaylen Bélanger** - *Programmeur*
  * IA du mage de feu, niveau Tutoriel, joueur
* **Gabriel Fortier** - *Programmeur*
  * Intégration du multijoueur, niveau 3, caméras (multijoueur)
* **Zachary Desrosby-Therrien** - *Programmeur*
  * Items, niveau 2, Achievements
