using Harmony;
using JetBrains.Annotations;
using System.Collections;
using UnityEngine;
using UnityEngine.InputSystem;

namespace Play {
    public class PlayerController : MonoBehaviour {

        [Tooltip("Player's jump grace time after leaving a platform")]
        [SerializeField]
        [Min(0)]
        private float coyoteTime = 0.15f;

        [Tooltip("Player's jump buffer time before hitting the floor")]
        [SerializeField]
        [Min(0)]
        private float jumpBufferTime = 0.1f;

        [Tooltip("Player's wall jump buffer time before sliding on a wall")]
        [SerializeField]
        [Min(0)]
        private float wallJumpBufferTime = 0.1f;

        [Tooltip("The duration during which the player won't be able to leave the wall's surface")]
        [SerializeField]
        [Min(0)]
        private float wallStickingTime = 0.1f;

        [Tooltip("The duration during which the player won't be able to slide down")]
        [SerializeField]
        [Min(0)]
        private float wallGrabTime = 0.5f;

        private new Rigidbody2D rigidbody;
        private new Collider2D collider;
        private Collider2D leftCollider;
        private Collider2D rightCollider;

        private ColliderUtils colliderUtils;
        private Mover mover;

        private float coyoteTimeCounter;
        private float jumpBufferCounter;
        private float wallJumpBufferCounter;
        private float gravityScale;
        private float movement;
        private float direction;

        private bool isWallSliding;
        private bool isWallJumping;
        private bool isWallGrabbing;
        private bool hasJustGrabbedRightWall;
        private bool hasJustGrabbedLeftWall;

        private void Awake() 
        {
            mover = GetComponent<Mover>();
            rigidbody = GetComponent<Rigidbody2D>();
            collider = GetComponent<Collider2D>();
            colliderUtils = GetComponent<ColliderUtils>();

            rightCollider = Finder.FindWithTag<Collider2D>(Tags.PlayerRightCollider).GetComponent<Collider2D>();
            leftCollider = Finder.FindWithTag<Collider2D>(Tags.PlayerLeftCollider).GetComponent<Collider2D>();

            gravityScale = rigidbody.gravityScale;
        }

        [UsedImplicitly]
        public void Jump(InputAction.CallbackContext context) 
        {
            if (context.performed) 
            {
                if (isWallSliding)
                {
                    StartCoroutine(WallJumpRoutine());
                } else
                {
                    StartCoroutine(JumpRoutine());
                }
            }

            if (context.canceled && rigidbody.velocity.y > 0 && !isWallJumping)
            {
                mover.CutJump(rigidbody);
                coyoteTimeCounter = 0;
            }
        }

        [UsedImplicitly]
        public void Move(InputAction.CallbackContext context) 
        {
            movement = Mathf.Round(context.ReadValue<float>());
        }

        private void FixedUpdate() 
        {
            if (colliderUtils.IsGrounded(collider))
            {
                coyoteTimeCounter = coyoteTime;
                hasJustGrabbedLeftWall = false;
                hasJustGrabbedRightWall = false;
                isWallSliding = false;
                isWallJumping = false;
                direction = 0;
            }
            else
            {
                coyoteTimeCounter -= Time.deltaTime;

                if (colliderUtils.IsColliding(rightCollider, Vector2.right))
                {
                    if (!hasJustGrabbedRightWall && rigidbody.velocity.y < 0)
                    {
                        StartCoroutine(GrabWallRoutine());
                        hasJustGrabbedRightWall = true;
                    }

                    WallSlide(-1);
                }
                else if (colliderUtils.IsColliding(leftCollider, Vector2.left))
                {
                    if (!hasJustGrabbedLeftWall && rigidbody.velocity.y < 0)
                    {
                        StartCoroutine(GrabWallRoutine());
                        hasJustGrabbedLeftWall = true;
                    }

                    WallSlide(1);
                }
                else
                {
                    StopCoroutine(GrabWallRoutine());
                    rigidbody.gravityScale = gravityScale;
                    isWallSliding = false;
                    isWallGrabbing = false;
                    direction = 0;
                }
            }

            if (rigidbody.velocity.y < 0 && !isWallSliding)
            {
                rigidbody.gravityScale = gravityScale * mover.FallGravityMultiplier;
            }

            if (Mathf.Abs(movement) < 0.01f)
            {
                float amount = Mathf.Min(Mathf.Abs(rigidbody.velocity.x), Mathf.Abs(mover.Friction));

                amount *= Mathf.Sign(rigidbody.velocity.x);

                rigidbody.AddForce(Vector2.right * -amount, ForceMode2D.Impulse);
            }
            
            if (!isWallSliding)
            {
                mover.Move(movement, rigidbody, collider);
            } else
            {
                StartCoroutine(UnstickToWallRoutine());
            }
        }

        private void WallSlide(float _direction)
        {
            isWallSliding = true;
            direction = _direction;

            if (rigidbody.velocity.y < 0 && !isWallGrabbing)
            {
                mover.WallSlide(rigidbody);
            }
        }

        public IEnumerator JumpRoutine()
        {
            jumpBufferCounter = jumpBufferTime;

            while (jumpBufferCounter > 0)
            {
                jumpBufferCounter -= Time.deltaTime;

                if (coyoteTimeCounter > 0)
                {
                    rigidbody.gravityScale = gravityScale;
                    mover.Jump(rigidbody);

                    jumpBufferCounter = 0;
                }
            }

            yield return null;
        }

        public IEnumerator WallJumpRoutine()
        {
            isWallJumping = true;
            wallJumpBufferCounter = wallJumpBufferTime;

            while (wallJumpBufferCounter > 0)
            {
                wallJumpBufferCounter -= Time.deltaTime;

                if (isWallSliding)
                {
                    if (direction == 1)
                    {
                        hasJustGrabbedLeftWall = false;
                    }
                    else if (direction == -1)
                    {
                        hasJustGrabbedRightWall = false;
                    }

                    rigidbody.gravityScale = gravityScale;
                    mover.WallJump(rigidbody, direction);
                    mover.CutJump(rigidbody);
                    wallJumpBufferCounter = 0;
                }
            }

            yield return null;
        }

        private IEnumerator GrabWallRoutine()
        {
            isWallGrabbing = true;
            rigidbody.gravityScale = 0;
            rigidbody.velocity = new Vector2(rigidbody.velocity.x, 0);

            yield return new WaitForSeconds(wallGrabTime);

            isWallGrabbing = false;
            rigidbody.gravityScale = gravityScale;
        }

        private IEnumerator UnstickToWallRoutine()
        {
            yield return new WaitForSeconds(wallStickingTime);

            mover.Move(movement, rigidbody, collider);
        }
    }
}