using UnityEngine;

namespace Play {
    public class Mover : MonoBehaviour {

        [Header("Movement")]
        [Tooltip("Character's horizontal speed")]
        [SerializeField]
        [Min(0)]
        private float moveSpeed = 14f;

        [Tooltip("Character's acceleration")]
        [SerializeField]
        [Min(0)]
        private float acceleration = 13f;

        [Tooltip("Character's deceleration")]
        [SerializeField]
        [Min(0)]
        private float deceleration = 20f;

        [Tooltip("Character's velocity power")]
        [SerializeField]
        [Min(0)]
        private float velPower = 0.96f;

        [Tooltip("Character's friction")]
        [SerializeField]
        [Min(0)]
        private float friction = 1.5f;

        public float Friction { get { return friction; } private set { } }

        [Tooltip("Character's jump force")]
        [SerializeField]
        [Min(0)]
        private float jumpForce = 20f;

        [Tooltip("Character's jump's cut multiplier")]
        [SerializeField]
        [Min(0)]
        private float jumpCutMultiplier = 0.4f;

        [Tooltip("Character's wall jump's vertical force")]
        [SerializeField]
        [Min(0)]
        private float wallJumpYForce = 16f;

        [Tooltip("Character's wall jump's horizontal force")]
        [SerializeField]
        [Min(0)]
        private float wallJumpXForce = 30f;

        [Tooltip("Player's wall sliding speed")]
        [SerializeField]
        [Min(0)]
        private float wallSlidingSpeed = 4f;

        [Tooltip("Character's fall gravity multiplier")]
        [SerializeField]
        [Min(0)]
        private float fallGravityMultiplier = 3f;

        public float FallGravityMultiplier { get { return fallGravityMultiplier; } private set { } }


        private ColliderUtils colliderUtils;

        private void Awake()
        {
            colliderUtils = GetComponent<ColliderUtils>();
        }

        public void Jump(Rigidbody2D rigidbody)
        {
            rigidbody.AddForce(Vector2.up * jumpForce, ForceMode2D.Impulse);
        }

        public void WallSlide(Rigidbody2D rigidbody)
        {
            rigidbody.velocity = new Vector2(rigidbody.velocity.x, -Mathf.Clamp(rigidbody.velocity.y, wallSlidingSpeed, float.MaxValue));
        }

        public void WallJump(Rigidbody2D rigidbody, float direction)
        {
            rigidbody.AddForce(Vector2.right * direction * wallJumpXForce, ForceMode2D.Impulse);
            rigidbody.AddForce(Vector2.up * wallJumpYForce, ForceMode2D.Impulse);
        }

        public void CutJump(Rigidbody2D rigidbody)
        {
            rigidbody.AddForce(Vector2.down * rigidbody.velocity.y * (1 - jumpCutMultiplier), ForceMode2D.Impulse);
        }

        public void Move(float direction, Rigidbody2D rigidbody2D, Collider2D collider2D) 
        {
            if (colliderUtils.CanMoveTo(direction, collider2D))
            {
                float targetSpeed = direction * moveSpeed;

                float speedDif = targetSpeed - rigidbody2D.velocity.x;

                float accelRate = (Mathf.Abs(targetSpeed) > 0.01f) ? acceleration : deceleration;

                float movement = Mathf.Pow(Mathf.Abs(speedDif) * accelRate, velPower) * Mathf.Sign(speedDif);

                rigidbody2D.AddForce(movement * Vector2.right);
            }
        }
    }
}