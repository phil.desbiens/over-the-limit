using Harmony;
using UnityEngine;

public class ColliderUtils : MonoBehaviour
{
    [Tooltip("Distance from an obstacle to sense")]
    [SerializeField]
    [Min(0)]
    private float obstacleDetectionRange = 0.02f;

    public bool IsGrounded(Collider2D collider2D)
    {
        return IsColliding(collider2D, Vector2.down);
    }

    public bool CanMoveTo(float direction, Collider2D collider2D)
    {
        return direction < 0 && !IsColliding(collider2D, Vector2.left)
               || direction > 0 && !IsColliding(collider2D, Vector2.right);
    }

    public bool IsColliding(Collider2D collider2D, Vector2 direction)
    {
        var bounds = collider2D.bounds;

        var raycast = Physics2D.BoxCast(bounds.center, bounds.size, 0f,
            direction, obstacleDetectionRange, Layers.Obstacle.Mask);

        return raycast.collider != null;
    }
}
